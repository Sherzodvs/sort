import java.io.*;

public class Sort {
    public static void main(String[] args) throws IOException {
        //FAYL PATH NI KO'RSATOLDI
        File file = new File("src/version.txt");

        //FAYLNI O'QOB OLINDI
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader
                = new BufferedReader(fileReader);
        //1 MLN UZUNLIDAGI STRING ARRAY OCHILDI
        String[] array = new String[1000000];
        int count = 0;
        String text;

        //FILE NI QATOR QILIB O'QIB NUQTALARNI TEKSHIRIB ARRAYGA YOZISH
        while ((text = bufferedReader.readLine()) != null){
            //STRING DA NECHTA NUQTA BORLIGINI SANAB QAYATARADI
            int countPoint = countPoint(text);

            //STRING DA NUQTA 5 TA YOKI UNDAN KAM BO'LSA ARRAYGA QO'SHADI
            if (countPoint <= 5) {
                array[count++] = text;
            }
        }

        //BIZDA NECHTA STRING YOZILGAN BO'LSA SHU UZUNLIKDA ARRAY OCHAMIZ
        String[]  newArray= new String[count];
        for (int i = 0; i < count; i++) {
            newArray[i] = array[i];
        }
        array = null;

        //ARRAYNI SORT METODIGA BERIB O'SISH TARTIBIDA SARALAYMIZ
        sort(newArray);

        //NATIJA sorted_version.txt FILE NI YARATIB OLAMIZ
        File result = new File("src/sorted_version.txt");
        FileWriter fileWriter = new FileWriter(result);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        //SARALANGAN ARRAYNI AYLANIB BUFFERED READER ORQALI FILEGA YOZAMIZ
        for (String s : newArray) {
            bufferedWriter.write(s);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();

        System.out.println("Ma'lumotlar o'sish taribida saralash yakunlandi");

    }
    //2 TA STRING BERAMIZ AGARDA 1-STRING KATTA BO'LSA TRUE, AKS XOLSA FALSE QAYATARDI
    public static boolean isMaxFirst(String a, String b) {
        //NUQTA BO'YICHA STRING NI SPLIT QILIB AJRATIB OLAMIZ
        String[] arrayA = a.split("\\.");
        String[] arrayB = b.split("\\.");

        //ENG KICHIK UZUNLIKDAGI SPLIT ARRAYNI UZUNLIGINI OLAMIZ
        int count = arrayA.length < arrayB.length ? arrayA.length : arrayB.length;

        for (int i = 0; i < count; i++) {
            int numberA = Integer.parseInt(arrayA[i]);
            int numberB = Integer.parseInt(arrayB[i]);
            if ( numberA > numberB){
                return true;
            } else if (numberA < numberB) {
                return false;
            }
        }
        return false;
    }

    //STRING DA NECHTA NUQTA BORLIGINI SANAB QAYATARADI
    public static int countPoint(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '.')
                count++;
        }
        return count;
    }

    //STRING ARRAY NI O'SHISH TARTIBIDA ARRAY O'ZIGA SARALAYDI
    /**
     * 2 TA KETMA KET KELGAN STRING NI TEKSHIRIB 1-STRING KATTA BO'LSA
     * 1- VA 2- STRING O'RNINI ALMASHTIRISH ORQALI ARRAY SARALANADI
     * @param array
     */
    public static void sort(String[] array) {
        boolean finished;
        String temp;
        do {
            finished = true;
            for (int i = 0; i < array.length - 1; i++)
            {
                //ISMAXFIRST METODIGA KETMA-KET STRING NI BERAMIZ 1-STRING KATTA BO'LSA TRUE, AKS HOLDA FALSE QAYTARADI
                if (isMaxFirst(array[i], array[i + 1]))
                {
                    finished = false;
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
        }
        while (!finished);

    }
}
